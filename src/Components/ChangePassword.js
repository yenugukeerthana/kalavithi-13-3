import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import  './ChangePassword.css';
import {Button, Modal} from 'react-bootstrap';

class ChangePassword extends Component {
   
  
    constructor(){
        super();
        this.state={
            value1:"",
            value2:"",
            currentPassword:"",
            show:false
        }
        this.handleClose=(event)=>{
            this.setState({
               show:false
            })
        }
        this.handleShow=()=>{
            this.setState({
               show:true
            })
        }

        this.handleChange1=(event)=>{
            this.setState({
                value1:event.target.value
            })
        }
        
        this.handleChange2=(event)=>{
            this.setState({
                value2:event.target.value
            })              
        }
        this.handleCurrent=(event)=>{
            this.setState({
                currentPassword:event.target.value
            })              
        }
        this.handleSubmit=async(event)=>{
            event.preventDefault();
        
            if ((this.state.value1 === this.state.value2) && ((this.state.value1!=="") && (this.state.value2!==""))
            && (this.state.currentPassword !== this.state.value1) && this.state.currentPassword!=="" ) {
           
    window.location.reload(true);
  }
};
  
 

    this.onCopy=(event) => {
        event.preventDefault();
        alert("Not allowed to copy password");
        return false;
      }
      this.onPaste=(event) => {
          event.preventDefault();
          return false;
        }
}

  render() {
      let colour1="black",colour2="black",colour3="black",colour4="black",colour5="black",colour6="black",colour7="black";
   

      if(this.state.value1.length >= "8")
      {
          colour1="green";
            
      }
      if(this.state.value1.match(/[A-Z]/))
      {
          colour2="green";    
           

      }
      if(this.state.value1.match(/[a-z]/))
      {
          colour3="green";   
       
 
      }
      if(this.state.value1.match(/[0-9]/))
      {
          colour4="green";    

      }
      if(this.state.value1.match(/[\d`~!@#$%\^&*()+=|;:'",.<>\/?\\\-]/)){
          colour5="green";
      }
      if(this.state.value1 === this.state.value2 && this.state.value1!=="" )
      {
          colour6="green";    
         

      }
      if(this.state.value1 !== this.state.currentPassword && this.state.currentPassword!=="" && this.state.value1!=="" )
      {
          colour7="green";    
           

      }
      
    
           
    return (
        <>
     <Button variant="primary" onClick={this.handleShow} style={{width:"150px"}}>  
       Change Password
      </Button>
 <Modal show={this.state.show} onHide={this.handleClose}>
 <Modal.Header closeButton>
   <Modal.Title   style={{color:"black"}}>Change your Password</Modal.Title>
 </Modal.Header>
 <Modal.Body> 
    <div className="container"> 
    <form class="style" onSubmit={this.handleSubmit}>  

    <div class="form-group">
            <label style={{color:"black"}} for="password">Current Password</label>
            <input type="password" class="form-control" value={this.state.currentPassword} onChange={this.handleCurrent} onCopy={this.onCopy} onPaste={this.onPaste} placeholder="Current Password"/>
          </div>
          <div class="form-group">
            <label style={{color:"black"}} for="password">Password</label>
            <input type="password" class="form-control" value={this.state.value1} onChange={this.handleChange1}   onCopy={this.onCopy} onPaste={this.onPaste} placeholder="Password"/>
          </div>
          <div class="form-group">
            <label style={{color:"black"}} for="password">Confirm Password</label>
            <input type="password" class="form-control" value={this.state.value2} onChange={this.handleChange2}    onCopy={this.onCopy} onPaste={this.onPaste} placeholder="Confirm Password"/>
          </div> 
         {this.state.value2 === "" ? "" :
         (this.state.value1 === this.state.value2  ? <p style={{color:"green",fontWeight:"bold"}}> Passwords match </p> :
         <p style={{color:"red",fontWeight:"bold"}}> Passwords not match </p>
          )}
          <div className='button'>

<button type="submit" className="btn btn-primary">Change password</button>
</div>

<div className='fields'>
          <p style={{fontWeight:"bold"}} > Password must have:</p>
          <p><i style={{color:colour1,fontSize:"20px"}} class="fa fa-check-circle" aria-hidden="true"></i> At least 8 characters</p>
          <p><i style={{color:colour2,fontSize:"20px"}} class="fa fa-check-circle" aria-hidden="true"></i> At least 1 uppercase letter</p>
          <p><i style={{color:colour3,fontSize:"20px"}} class="fa fa-check-circle" aria-hidden="true"></i> At least 1 lowercase letter</p>
          <p><i style={{color:colour4,fontSize:"20px"}} class="fa fa-check-circle" aria-hidden="true"></i> At least 1 number</p>
          <p><i style={{color:colour5,fontSize:"20px"}} class="fa fa-check-circle" aria-hidden="true"></i> At least 1 speacial character</p>
          <p><i style={{color:colour6,fontSize:"20px"}} class="fa fa-check-circle" aria-hidden="true"></i> Password should be same as Confirm Password</p>
          <p><i style={{color:colour7,fontSize:"20px"}} class="fa fa-check-circle" aria-hidden="true"></i> Password does not equal to Current Password</p>
        
          </div>
    </form>
    </div>
   
   
        
    <div className="col-md-4"></div>

    </Modal.Body>
      </Modal>
      </>
    );
  }
}

export default ChangePassword;