import React, {useEffect, useState} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button, Modal} from 'react-bootstrap';
import UserRegistration from './UserRegistrationPage';
import ChangePassword from './ChangePassword';

const LoginPage = (props) => {  
    // {showLogin, handleLoginClose}
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");

  const [passwordError, setpasswordError] = useState("");
  const [emailErrorLogin, setemailErrorLogin] = useState("");
  const [show, setShow] = useState(false);

  const handleClose = () => {
    setShow(false);
  }
  const handleShow = () => {
    setShow(true);
  }

  const handleValidation = (event) => {
    let formIsValid = true;

    if (!email.match(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)) {
      formIsValid = false;
      setemailErrorLogin("Email Not Valid");
      return false;
    } else {
      setemailErrorLogin("");
      formIsValid = true;
    }

    if (!password.match(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,15}$/)) {
      formIsValid = false;
      setpasswordError(
        "Only Letters and length must best min 8 Chracters and Max 15 Chracters"
      );
      return false;
    } else {
      setpasswordError("");
      formIsValid = true;
    }

    return formIsValid;
  };

  
  let handleLoginSubmit = async (e) => {
    e.preventDefault();
    if(email==""){
      setemailErrorLogin("email can not be empty");
      return;
    }else{
      setemailErrorLogin("");
    }
    if(password==""){
      setpasswordError("password can not be empty");
      return;
    }else{
      setpasswordError("");
    }
    
    if(!handleValidation()){
      return;
    }
    for (let i = 0; i < users.length; i++) {
      if (users[i].email === email) {
        return;
      }
      setemailErrorLogin("User doesn't exists");
    }
    const url = 'http://localhost:8090/api/users';
    try {
      let res = await fetch(url, {
        method: "POST",
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ 
           'email': email, 
           'user_password': password
        }),
      });
      if (res.status === 200) {
        setEmail("");
        setPassword("");
        console.log("User Login successfully");
      } else {
        console.log("Some error occured");
      }
    } catch (err) {
      console.log(err);
    }
  };

  const [users, setUsers] = useState([]);
  const getUsersRequest = async () => {
    const url = 'http://localhost:8090/api/users';

    const response = await fetch(url);
    const responseJson = await response.json();
    console.log(responseJson);
    setUsers(responseJson);
  };
  

  useEffect(() => {
    getUsersRequest();
  }, []);

  return <>
      <Modal show={props.showLogin} onHide={props.handleLoginClose}>
        <Modal.Header closeButton>
          <Modal.Title   style={{color:"black"}}>User Login</Modal.Title>
         </Modal.Header>
        <Modal.Body>
         <form id="loginform" onSubmit={handleLoginSubmit}>
              <div className="form-group">
               <label style={{color:"black" }}>Username</label>
                <input
                  type="email"
                  className="form-control"
                  id="EmailInput"
                  name="EmailInput"
                  aria-describedby="emailHelp"
                  placeholder="Enter email"
                  onChange={(event) => setEmail(event.target.value)}
                />
                <small id="emailHelp" className="text-danger form-text">
                  {emailErrorLogin}
                </small>
              </div>
              <div className="form-group mt-4">
                <label style={{color:"black"}}>Password</label>
                <input
                  type="password"
                  className="form-control"
                  id="exampleInputPassword1"
                  placeholder="Password"
                  onChange={(event) => setPassword(event.target.value)}
                />
                <small id="passworderror" className="text-danger form-text">
                  {passwordError}
                </small>
              </div>
              
              <Button variant="primary" onClick={handleShow} style={{width:"150px",float:"left"}}  >
         Login
      </Button>
       <button style={{width:"300px",float:"right"}} id="changepassword_button"><ChangePassword/></button>
      <UserRegistration show={show} handleClose={handleClose}/>
              <h5 id="anchortag">New User! Register<a onClick={handleShow} style={{color:"red"}}> here </a>
</h5>
            </form>
        </Modal.Body>
        </Modal>


  </>; 
};



export default LoginPage;

