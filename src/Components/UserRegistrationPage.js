import React, {useEffect, useState} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Modal} from 'react-bootstrap';

const UserRegistration = (props) => {  

  const [fullname, setFullname] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");

  const [passwordError, setpasswordError] = useState("");
  const [emailError, setemailError] = useState("");
  const [fullnameError, setfullnameError] = useState("");

  const handleValidation = (event) => {
    let formIsValid = true;

    if (!email.match(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)) {
      formIsValid = false;
      setemailError("Email Not Valid");
      return false;
    } else {
      setemailError("");
      formIsValid = true;
    }

    if (!password.match(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,15}$/)) {
      formIsValid = false;
      setpasswordError(
        "Only Letters and length must best min 8 Chracters and Max 15 Chracters"
      );
      return false;
    } else {
      setpasswordError("");
      formIsValid = true;
    }

    return formIsValid;
  };

  
  let handleSubmit = async (e) => {
    e.preventDefault();
    if(fullname==""){
      setfullnameError("usename can not be empty");
      return;
    }else{
      setfullnameError("");
    }
    if(email==""){
      setemailError("email can not be empty");
      return;
    }else{
      setemailError("");
    }
    if(password==""){
      setpasswordError("password can not be empty");
      return;
    }else{
      setpasswordError("");
    }
    
    if(!handleValidation()){
      return;
    }
    for (let i = 0; i < users.length; i++) {
      if (users[i].email === email) {
        setemailError("Email alreday exists");
        return;
      }
    }
    const url = 'http://localhost:8090/api/users';
    try {
      let res = await fetch(url, {
        method: "POST",
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ 
           'fullname': fullname, 
           'email': email, 
           'user_password': password
        }),
      });
      if (res.status === 200) {
        setFullname("");
        setEmail("");
        setPassword("");
        console.log("User created successfully");
      } else {
        console.log("Some error occured");
      }
    } catch (err) {
      console.log(err);
    }
  };

  const [users, setUsers] = useState([]);
  const getUsersRequest = async () => {
    const url = 'http://localhost:8090/api/users';

    const response = await fetch(url);
    const responseJson = await response.json();
    console.log(responseJson);
    setUsers(responseJson);
  };
  

  useEffect(() => {
    getUsersRequest();
  }, []);

  return <>
    <Modal show={props.show} onHide={props.handleClose}>
        <Modal.Header closeButton>
          <Modal.Title   style={{color:"black"}}>Create your account</Modal.Title>
        </Modal.Header>
        <Modal.Body>
         <form id="registrationform" onSubmit={handleSubmit}>
             <div className="form-group">
               <label style={{color:"black"}}>Name</label>
                <input

                  type="text"
                  className="form-control"
                  id="NameInput"
                  name="NameInput"
                  placeholder="Enter name"
                  value={fullname}
                  onChange={(event) => setFullname(event.target.value)}
                />
                <small id="fullnameHelp" className="text-danger form-text">
                  {fullnameError}
                </small>
              </div>
              <div className="form-group mt-4">
               <label style={{color:"black"}}>Email</label>
                <input
                  type="email"
                  className="form-control"
                  id="EmailInput"
                  name="EmailInput"
                  aria-describedby="emailHelp"
                  placeholder="Enter email"
                  value={email}
                  onChange={(event) => setEmail(event.target.value)}
                />
                <small id="emailHelp" className="text-danger form-text">
                  {emailError}
                </small>
              </div>
              <div className="form-group mt-4">
                <label style={{color:"black"}}>Password</label>
                <input
                  type="password"
                  className="form-control"
                  id="exampleInputPassword1"
                  placeholder="Password"
                  value={password}
                  onChange={(event) => setPassword(event.target.value)}
                />
                <small id="passworderror" className="text-danger form-text">
                  {passwordError}
                </small>
              </div>
              
              <button type="submit" className="btn btn-primary mt-5">
                Register
              </button>
            </form>
        </Modal.Body>
      </Modal>
  </>; 
};

export default UserRegistration;
