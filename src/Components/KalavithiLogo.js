import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

const Logo = () => {
    return (
    <div className="col">
          <img
            className='rounded-circle'
            src={process.env.PUBLIC_URL+"/kalavithi_favicon_transparent.png"}
            alt="Open"
            width={60}
            height={80} />
    </div>
  );
}

export default Logo;