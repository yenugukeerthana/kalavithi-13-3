import React, { useState } from "react";


var redHeart=0;

const AddFavourites = () => {
  const [style, setStyle] = useState("fa fa-heart-o");

 
  
  const [count, setCount] = useState(0);

  const isClicked = () => {
    if (redHeart === 0) {
      redHeart = 1;
      console.log(redHeart);
      setStyle("fa fa-heart red-color");
      setCount((prevCount) => prevCount + 1);
    } else{
      redHeart = 0;
      console.log(redHeart);
      setStyle("fa fa-heart-o");
      setCount((prevCount) => prevCount - 1);
    }
  };

  return (
    <>
      <button
        className="like-btn"
        title="likeButton"
        onClick={isClicked}
        name="like"
      >
        <i className={style} title={style} aria-hidden="true"></i>
      </button>
      <span className="mr-2" title="counter">
	  {redHeart > 0 ? redHeart + " Like" : " Like"}{" "}

      </span>
    </>
  );
};

export default AddFavourites;