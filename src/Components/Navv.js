import React, {useEffect, useState} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button, Modal, NavDropdown} from 'react-bootstrap';
import UserRegistration from './UserRegistrationPage';
import ChangePassword from './ChangePassword';
import { ReactDOM } from "react";
import LoginPage from './Login';
import {BiUserCircle} from 'react-icons/bi';
import {BiLogOutCircle} from 'react-icons/bi';
import {BiUserPlus} from 'react-icons/bi';
import KalaListHeading from './KalaListHeading';
import SearchBox from './SearchBox';
import Logo from './KalavithiLogo';
import Setting from './Settings';

const Navv = () => {
  const [show, setShow] = useState(false);
  const [showSetting, setShowSetting] = useState(false);
  const [showLogin, setShowLogin] = useState(false);


  const handleClose = () => {
    setShow(false);
  }
  const handleShow = () => {
    setShow(true);
  }

  const handleValidation = (event) => {
    let formIsValid = true;
    return formIsValid;
  };

  const handleShowSetting = () => {
    setShowSetting(true);
    // setShowLogin(false);
  }
  const handleCloseSetting = () => {
    setShowSetting(false);
    // setShowLogin(true);
  }
  const handleLoginClose = () => {
    setShowLogin(false);
    // setShow(false);
  }
  const handleLoginShow = () => {
    setShowLogin(true);
  // setShow(false);
  }

  return (
  <div>
    <Logo/>
    <SearchBox/>
        <div id="nav2">
        <NavDropdown title="Profile" id="navbarprofile" > 
          <NavDropdown.Item>
          <Button variant="primary" onClick={handleShow} style={{width:"150px"}} id="btn4"><BiUserPlus size="20px"/>
         Registration
      </Button>
      <div id="navbar_button"><UserRegistration show={show} handleClose={handleClose}/></div>
          </NavDropdown.Item>

          <NavDropdown.Item>
          <Button variant="primary" onClick={handleShowSetting} style={{width:"150px"}} id="btn4"><BiUserCircle size="20px"/>
         Settings
         </Button>
         <div id="navbar_button"><Setting showSetting={showSetting} handleCloseSetting={handleCloseSetting}/></div>
          </NavDropdown.Item>
          <NavDropdown.Item >
          <Button variant="primary" onClick={handleLoginShow} style={{width:"150px"}} id="btn4"><BiUserCircle size="20px"/>Login
            </Button>
            <div id="navbar_button"><LoginPage showLogin={showLogin} handleLoginClose={handleLoginClose}/></div>
            </NavDropdown.Item>
        </NavDropdown>
        </div>
</div>

);
}

export default Navv;