import React, {useEffect, useState} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import KalaList from './Components/KalaList';

import SearchBox from './Components/SearchBox';
import AddFavourite from './Components/AddFavourites';
import KalavithiLogo from './Components/KalavithiLogo';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button, Modal} from 'react-bootstrap';
import { Navbar,NavDropdown} from 'react-bootstrap';
import {BiUserCircle} from 'react-icons/bi';
import {BiLogOutCircle} from 'react-icons/bi';
import {BiUserPlus} from 'react-icons/bi';
import ChangePassword from './Components/ChangePassword';
import LoginPage from './Components/Login';
import UserRegistrationPage from './Components/UserRegistrationPage';
import Navigation from './Components/Navigation';

const App = () => {
  const [show, setShow] = useState(false);
  const [showSetting, setShowSetting] = useState(false);
  const [showLogin, setShowLogin] = useState(false);
  const [fullname, setFullname] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");

  const [passwordError, setpasswordError] = useState("");
  const [emailError, setemailError] = useState("");
  const [fullnameError, setfullnameError] = useState("");

  const handleClose = () => {
    setShow(false);
    // setShowLogin(true);
  }
  const handleShow = () => {
    setShow(true);
    // setShowLogin(false);
  }
  const handleShowSetting = () => {
    setShowSetting(true);
    // setShowLogin(false);
  }
  const handleCloseSetting = () => {
    setShowSetting(false);
    // setShowLogin(true);
  }
  const handleLoginClose = () => {
    setShowLogin(false);
    // setShow(false);
  }
  const handleLoginShow = () => {
    setShowLogin(true);
  // setShow(false);
  }

  const [passwordLogin, setPasswordLogin] = useState("");
  const [emailLogin, setEmailLogin] = useState("");
  const [passwordErrorLogin, setpasswordErrorLogin] = useState("");
  const [emailErrorLogin, setemailErrorLogin] = useState("");

  const [username, setUsername] = useState("");


  const [kalas, setKalas] = useState(
    []
  );
    const getMovieRequest = async () => {
      const url = 'https://kalavithi-service-aquila-dev.herokuapp.com/api/images';

      const response = await fetch(url);
      const responseJson = await response.json();
      console.log(responseJson);
      setKalas(responseJson.images);
    };

    useEffect(() => {
      getMovieRequest();
    }, []);

  return <div className='container-fluid kala-app'>
    <div className="logo">
      <Navigation/>
</div>

    <div className='d-flex flex-wrap'>
      <KalaList kalas={kalas} favouriteComponent = {AddFavourite}/>
    </div>

  </div>; 
}

export default App;
